package trace

import (
	"io"
)

type Tracer interface {
	Trace(*io.Reader)
	Dispatch(*Filter)
}
