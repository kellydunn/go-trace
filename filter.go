package trace

// A filter defines the mechanism that processes
// Single event and may or may not signal the tracer
// to dispatch a message
type Filter interface {
	Process(*Event)
}
