package trace

import (
	"time"
)

// Describes a single event inside of the tracing system.
// This could represent a single log message in syslog, or a
// carraige return delimited line in any other logging system, etc.
type Event struct {
	timestamp time.Time
	body      []byte
}

func NewEvent(body []byte) *Event {
	event := &Event{body: body}
	return event
}

func (e *Event) Body() []byte {
	return e.body
}
