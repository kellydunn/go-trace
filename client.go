package trace

// A client receives tracing messages.
type Client interface {
	Recv(*Message)
}
