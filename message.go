package trace

// A message sent to a consumer of the Tracer
type Message struct {
	events []*Event
}
